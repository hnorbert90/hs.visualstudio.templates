﻿namespace $rootnamespace$
{	
	using HS.Mediator.Common;
    public class $queryname$QueryResponse : ListQueryResponseBase<$queryname$QueryResponse.$queryname$DTO>
    {
        public class $queryname$DTO
        {
        }
    }
}