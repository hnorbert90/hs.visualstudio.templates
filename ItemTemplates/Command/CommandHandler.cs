﻿namespace $rootnamespace$
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
		
	using HS.Mediator.Common;
    public class $commandname$CommandHandler : HandlerBase<$commandname$Command, $commandname$CommandResponse>
    {
        public $commandname$CommandHandler()
        {
        }

        public override Task<$commandname$CommandResponse> Handle($commandname$Command request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}